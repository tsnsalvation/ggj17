﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Product
{
    public string productName;
    public int productCost;
    public int productTopValue;
    public bool availableFromShop;
    public AnimationCurve valueOverTime = new AnimationCurve(new Keyframe(0,0), new Keyframe(15, 1), new Keyframe(30,0));
    public List<CookingEffect> effects;

    public override string ToString()
    {
        return productName.ToString();
    }
    public  bool Equals(Product obj)
    {
        return obj.productName == this.productName;
    }

    public static bool operator ==(Product a,Product b)
    {
        return a.Equals(b);
    }
    public static bool operator !=(Product a, Product b)
    {
        return a.Equals(b);
    }
    //public override int GetHashCode()
    //{
    //    unchecked
    //    {
    //        int hash = target.GetHashCode();

    //        return hash;
    //    }
    //}
}
[System.Serializable]
public class CookingEffect
{
    public string name;
    public EffectTypes effectType;
    public float triggerDuration;
}
[System.Serializable]

public enum EffectTypes
{
    Smoke,
    Fire,
    Sparks
}