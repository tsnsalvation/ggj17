﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class PurchaseProduct : MonoBehaviour
{

    public Product prod;
    public int count;
    public Text quantityText;
    public ShoppingHud shopHud;
    public Image imageSprite;

    public void AddProd()
    {
        //can I afford 1?
        if (shopHud.AddToOrder(prod))
        {
            count++;
            quantityText.text = count.ToString();
            
        }

    }
    public void RemoveProd()
    {
        //Can i remove?
        if (count > 0)
        {
            count--;
            quantityText.text = count.ToString();
            shopHud.RemoveFromOrder(prod);
        }
    }
    public void ResetCount()
    {
        count = 0;
        quantityText.text = "0";
    }
}
