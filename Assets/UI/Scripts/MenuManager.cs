﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    bool creditsShown = false;
    public GameObject creditsPanel;
    // Use this for initialization
    void Start()
    {
        creditsPanel.SetActive(creditsShown);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }



    public void ToggleCredits()
    {
        creditsShown = !creditsShown;
        creditsPanel.SetActive(creditsShown);

    }
}
