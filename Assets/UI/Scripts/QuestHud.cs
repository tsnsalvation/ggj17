﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestHud : MonoBehaviour {

    public Text questChalkboard;

    public Text moneyRequired;

    public void BuildQuestText(QuestList newQuests)
    {
        string questText = "";
        for (int i = 0; i < 5; i++)
        {
            if (i == newQuests.quests.Count)
                break;
            questText += string.Format("{0}x COOKED {1}    REWARD: {2}\n", newQuests.quests[i].quantity, newQuests.quests[i].product.productName, newQuests.quests[i].bonus);
        }
        questChalkboard.text = questText;
    }
    public void SetMoneyRequired(int requireMoney)
    {
        moneyRequired.text = string.Format("REQUIRED MONEY: ${0}", requireMoney);
    }
}
