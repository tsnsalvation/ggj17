﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WorldProducts : ScriptableObject
{
    
    public List<Product> products;
}
