﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShoppingHud : MonoBehaviour
{

    public GameManager gameMgr;
    public GameObject shoppingPanel;

    public GameObject shoppingPanelGroup;

    public ShoppingOrder order;

    public GameObject orderItemPrefab;

    public WorldProducts productDatabase;

    public string productSpriteFolder = "Sprite/";

    public GameObject trigger;

    void Start()
    {
        PopulateList();
        //gameObject.SetActive(false);
    }

    void PopulateList()
    {
        foreach(var prod in productDatabase.products)
        {
            if (prod.availableFromShop)
            {
                try
                {
                    var go = Instantiate(orderItemPrefab, shoppingPanel.transform);

                    var purchProd = go.GetComponent<PurchaseProduct>();
                    purchProd.prod = prod;
                    purchProd.imageSprite.sprite = Resources.Load<Sprite>(productSpriteFolder + prod.productName);
                    purchProd.shopHud = this;

                }
                catch (System.Exception)
                {
                    //Lazy code

                }
            }
        }
    }

    public void EnableShopping()
    {
        trigger.SetActive(false);
        shoppingPanelGroup.SetActive(true);
        gameMgr.DisablePlayer();
    }

    public void DisableShopping()
    {
        
        shoppingPanelGroup.SetActive(false);
        gameMgr.EnablePlayer();
        StartCoroutine(EnableOrderForm());
    }
    IEnumerator EnableOrderForm()
    {
        yield return null;
        trigger.SetActive(true);
    }
    
    public bool AddToOrder(Product prod)
    {
        if (gameMgr.CurrentMoney - prod.productCost >0)
        {
            gameMgr.CurrentMoney -= prod.productCost;
            order.orderedProducts.Add(prod);
            return true;
        }

        return false;
    }
    public void RemoveFromOrder(Product prod)
    {
        gameMgr.CurrentMoney += prod.productCost;
        order.orderedProducts.Remove(prod);
    }
}
