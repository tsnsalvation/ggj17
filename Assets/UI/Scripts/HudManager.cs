﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class HudManager : MonoBehaviour
{
    public Text timerText;
    public Text moneyText;
    public Text phaseText;
    public Text roundText;
    public Text interactText;
    public Text rightpromptText;

    public GameObject playingHud;
    public GameObject gameOver;
    public GameObject quitMenu;
    bool quitShowing = false;
    public Text gameInfoText;
    public GameManager gameMgr;


    public Image cursor;
    public bool cursorInteractive;
    void Update()
    {
        cursor.transform.position = Input.mousePosition;

        if (cursorInteractive)
        {
            cursor.color = Color.green;
        }
        else
        {
            cursor.color = Color.white;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            quitShowing = !quitShowing;
            if (quitShowing)
            {
                
                gameMgr.DisablePlayer();
            }
            else
            {
                gameMgr.EnablePlayer();
            }
            quitMenu.SetActive(quitShowing);
        }
    }

    public void UpdatePhase(string phase)
    {
        phaseText.text = phase.ToUpper();
    }
    public void UpdateTime(float time)
    {
        timerText.text = string.Format("{0}:{1:00}", (int)time / 60, (int)time % 60);
    }

    public void UpdateMoney(int money)
    {
        moneyText.text = string.Format("${0}", money);
    }
    public void UpdateRound(int roundNum)
    {
        roundText.text = string.Format("Round: {0}", roundNum);
    }

    public void GameOver()
    {
        playingHud.SetActive(false);

        gameInfoText.text = string.Format("Congratulations. You managed to make {0}!", moneyText.text);
        gameOver.SetActive(true);
    }

    public void ExitGame()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        //Application.Quit();
    }

}
