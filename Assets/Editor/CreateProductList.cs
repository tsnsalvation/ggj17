﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateProductList
{
    [MenuItem("Assets/Create/Inventory Item List")]
    public static WorldProducts Create()
    {
        WorldProducts asset = ScriptableObject.CreateInstance<WorldProducts>();

        AssetDatabase.CreateAsset(asset, "Assets/ProductList.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}