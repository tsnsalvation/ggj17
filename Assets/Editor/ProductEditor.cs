﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class ProductEditor : EditorWindow
{

    public WorldProducts productDatabase;
    private int productIndex;

    [MenuItem("Window/Product Editor %#e")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(ProductEditor));
    }

    void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            productDatabase = AssetDatabase.LoadAssetAtPath(objectPath, typeof(WorldProducts)) as WorldProducts;
        }

    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Product Editor", EditorStyles.boldLabel);

        if (productDatabase != null)
        {
            if (GUILayout.Button("Save Products List"))
            {
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(productDatabase);
                AssetDatabase.SaveAssets();
            }
        }
        if (GUILayout.Button("Open Product List"))
        {
            OpenProductList();
        }
        if (GUILayout.Button("New Product List"))
        {
            CreateNewProductList();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(20);

        if (productDatabase != null)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Space(10);
            if (GUILayout.Button("First", GUILayout.ExpandWidth(false)))
            {

                productIndex = 1;
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
            {
                if (productIndex > 1)
                    productIndex--;
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
            {
                if (productIndex < productDatabase.products.Count)
                {
                    productIndex++;
                }
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Last", GUILayout.ExpandWidth(false)))
            {
                productIndex = productDatabase.products.Count;

            }
            GUILayout.Space(20);

            if (GUILayout.Button("Add Product"))
            {
                AddProduct();
            }
            if (GUILayout.Button("Delete Product"))
            {
                DeleteProduct(productIndex - 1);
            }

            GUILayout.EndHorizontal();

            GUILayout.Space(20);
            if (productDatabase.products == null)
            {
                if (GUILayout.Button("InitialiseDB", GUILayout.ExpandWidth(false)))
                {
                    productDatabase.products = new List<Product>();

                }
            }
            else
            {
                if (productDatabase.products.Count > 0)
                {
                    GUILayout.BeginHorizontal();
                    productIndex = Mathf.Clamp(EditorGUILayout.IntField("Current Product", productIndex, GUILayout.ExpandWidth(false)), 1, productDatabase.products.Count);
                    Mathf.Clamp(productIndex, 1, productDatabase.products.Count);
                    EditorGUILayout.LabelField("of   " + productDatabase.products.Count.ToString() + "  products", "", GUILayout.ExpandWidth(false));
                    GUILayout.Space(10);

                    GUILayout.EndHorizontal();
                    GUILayout.Space(10);
                    productDatabase.products[productIndex - 1].productName = EditorGUILayout.TextField("Product Name", productDatabase.products[productIndex - 1].productName as string);
                    productDatabase.products[productIndex - 1].productCost = EditorGUILayout.IntField("Prouct Cost", productDatabase.products[productIndex - 1].productCost);
                    productDatabase.products[productIndex - 1].productTopValue = EditorGUILayout.IntField("Product Top Value", productDatabase.products[productIndex - 1].productTopValue);
                    productDatabase.products[productIndex - 1].valueOverTime = EditorGUILayout.CurveField("Value over time", productDatabase.products[productIndex - 1].valueOverTime);
                    productDatabase.products[productIndex - 1].availableFromShop = EditorGUILayout.Toggle("Available from shop", productDatabase.products[productIndex - 1].availableFromShop);


                    GUILayout.Space(10);
                    for (int i = 0; i < productDatabase.products[productIndex - 1].effects.Count; i++)
                    {
                        var pe = productDatabase.products[productIndex - 1].effects[i];

                        pe.name = EditorGUILayout.TextField("Effect Name", pe.name);
                        pe.effectType = (EffectTypes)EditorGUILayout.EnumPopup("Effect Type", pe.effectType);
                        pe.triggerDuration = EditorGUILayout.FloatField("Duration to trigger", pe.triggerDuration);

                    }
                }
                else
                {
                    GUILayout.Label("This Product List is Empty.");
                }
            } 
        }
    }

    void AddProduct()
    {
        Product prod = new Product();
        prod.productName = "New Product";
        prod.effects = new List<CookingEffect>();
        productDatabase.products.Add(prod);
        productIndex = productDatabase.products.Count;
    }

    void DeleteProduct(int index)
    {
        productDatabase.products.RemoveAt(index);
    }
    void CreateNewProductList()
    {
        productIndex = 1;
        productDatabase = CreateProductList.Create();
        if (productDatabase)
        {
            productDatabase.products = new List<Product>();
            string relPath = AssetDatabase.GetAssetPath(productDatabase);
            EditorPrefs.SetString("ObjectPath", relPath);
        }
    }
    void OpenProductList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Product Item List", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            productDatabase = AssetDatabase.LoadAssetAtPath(relPath, typeof(WorldProducts)) as WorldProducts;
            if (productDatabase.products == null)
                productDatabase.products = new List<Product>();
            if (productDatabase)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }
}
