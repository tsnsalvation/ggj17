﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class InitialProductEditor : EditorWindow
{

    public WorldProducts productDatabase;
    public ShoppingOrder shoppingOrder;
    private int productIndex;

    private List<int> selectedIndex = new List<int>();
    private List<int> selectedQuantity = new List<int> ();


    [MenuItem("Window/Intial Product Editor %#i")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(InitialProductEditor));
    }

    void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            productDatabase = AssetDatabase.LoadAssetAtPath(objectPath, typeof(WorldProducts)) as WorldProducts;
        }
        
    }

    void OnGUI()
    {

        GUILayout.Label("Initial Product Editor", EditorStyles.boldLabel);
        shoppingOrder = (ShoppingOrder)EditorGUILayout.ObjectField("Shopping Order", shoppingOrder, typeof(ShoppingOrder),true);

        if (productDatabase != null && shoppingOrder != null)
        {
            if (GUILayout.Button("Add Product", GUILayout.ExpandWidth(false)))
            {
                InitialOrderItem p = new InitialOrderItem();
                p.product = new Product();
                p.qty = 1;
                shoppingOrder.initialOrder.Add(p);
                selectedIndex.Add(0);
                selectedQuantity.Add(1);
            }
            for (int i = 0;i < shoppingOrder.initialOrder.Count; i++)
            {
                if (shoppingOrder.initialOrder.Count != selectedIndex.Count || shoppingOrder.initialOrder.Count != selectedQuantity.Count)
                    FixMisalignedIndex();

                var prod = shoppingOrder.initialOrder[i];
                GUILayout.BeginHorizontal();
                //var prod in shoppingOrder.initialOrde
                //product name
                selectedIndex[i] = EditorGUILayout.Popup(selectedIndex[i], GetProductName());

                //quantity
                selectedQuantity[i] = EditorGUILayout.IntField(selectedQuantity[i]);

                //delete button
                if (GUILayout.Button("-", GUILayout.ExpandWidth(false)))
                {
                    shoppingOrder.initialOrder.RemoveAt(i);
                    selectedIndex.RemoveAt(i);
                    selectedQuantity.RemoveAt(i);
                }
                if (productDatabase.products != null && productDatabase.products.Count != 0)
                {
                    shoppingOrder.initialOrder[i].product = productDatabase.products[selectedIndex[i]];
                    shoppingOrder.initialOrder[i].qty = selectedQuantity[i];

                }
                GUILayout.EndHorizontal();
            }
        }
        if (productDatabase == null)
        {
            GUILayout.Label("Product List is Empty");
        }
    }
    string[] GetProductName()
    {
        List<string> prodNames = new List<string>();
        if (productDatabase.products != null && productDatabase.products.Count != 0)
        {
            
            foreach (var p in productDatabase.products)
            {
                prodNames.Add(p.ToString());
            }
            
        }
        else
        {
            prodNames.Add("");

        }
        return prodNames.ToArray();
    }
    void FixMisalignedIndex()
    {
        for (int i = 0; i < shoppingOrder.initialOrder.Count; i++)
        {
            if (selectedIndex.Count < i + 1)
            {
                if (shoppingOrder.initialOrder[i].product != null)
                {
                    var index = productDatabase.products.IndexOf(shoppingOrder.initialOrder[i].product);
                    if (index < 0)
                        index = 0;
                    selectedIndex.Add(index);
                }
                else
                {
                    selectedIndex.Add(0);
                }
            }
            if (selectedQuantity.Count < i + 1)
            {
                selectedQuantity.Add(shoppingOrder.initialOrder[i].qty);
            }
        }
    }
}
