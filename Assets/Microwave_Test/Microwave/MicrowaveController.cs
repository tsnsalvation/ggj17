﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MicrowaveController : MonoBehaviour {
	public float TimeM;
	public string DisplayTime;
	public bool MicrowaveON;
	PlayerController playerCon;
	public bool OpenDoor;
	public float MaxTime;
	public Text DisplayText;
	public Transform doorHinge;
	public Quaternion desiredRot;
	public float doorSpeed;
	public GameObject[] micSpots;
	public Transform[] spotTrans;
	public bool empty;
	public int DoorRot;
	public float timeCooked;
    public AudioSource dingSound;
	// Use this for initialization
	void Start () 
	{
		playerCon = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		DisplayText.text = string.Format ("{0}:{1:00}", (int)TimeM / 60, (int)TimeM % 60);
		if (OpenDoor) 
		{
			doorHinge.rotation = Quaternion.Lerp (doorHinge.rotation, Quaternion.Euler(new Vector3(0,DoorRot,0)), Time.deltaTime * doorSpeed);
		} 
		else 
		{
			doorHinge.rotation = Quaternion.Lerp (doorHinge.rotation, Quaternion.Euler(new Vector3(0,0,0)),Time.deltaTime * doorSpeed);
		}

	}

	void FixedUpdate()
	{


		if (MicrowaveON) 
		{
			if (TimeM <= 0.3) 
			{
				//insert ding and effect trigger

				MicrowaveON = false;
                dingSound.Play();
            }
			TimeM -= Time.deltaTime;
			timeCooked += Time.deltaTime;
			// recipes change product
			foreach (GameObject cookingProducts in micSpots) 
			{
				if (cookingProducts != null) 
				{
					var product = cookingProducts.GetComponent<InteractiveProduct> ();
					product.cookTime = product.thisProduct.valueOverTime.Evaluate (timeCooked);

					if (product.cookTime >= 0.85f) 
					{
						product.productState = 1;
					} else if 
						(product.cookTime <= 0.85f && product.productState == 1)
						product.productState = 2;
                    product.worth = (int)(product.thisProduct.productCost * product.cookTime);
                }
			}
			



		}

		if (micSpots [0] == null && micSpots [1] == null && micSpots [2] == null)
			empty = true;
		else
			empty = false;




	}

	public void addTime(int ammount)
	{
		if (TimeM <= 0) 
		{
			TimeM += ammount;
		} else 
		{
			if (TimeM <= 9999) 
			{
//				TimeM *= 10;
				TimeM += ammount;
			}

		}


	}

	public void addObject(GameObject ItemToAdd ,int spotItem)
	{
//		int spotItem;
//		spotItem = Random.Range (0, micSpots.Length);
		if (micSpots [spotItem] != null) 
		{
			addObject (ItemToAdd ,spotItem += 1);

		}
		else
		{
			ItemToAdd.transform.position = spotTrans [spotItem].position;
			ItemToAdd.transform.rotation = Quaternion.Euler (0, 0, 0);
			ItemToAdd.transform.parent = spotTrans [spotItem];
			micSpots[spotItem] = ItemToAdd; 
		}

	}

	public void TurnOn()
	{
		if (!OpenDoor)
		MicrowaveON = true;

	}

	public void Eject()
	{
		
		if (!OpenDoor) 
		{
			MicrowaveON = false;
			OpenDoor = true;

		} 


	}
	public void CloseDoor()
	{
//		if(OpenDoor)
		OpenDoor = !OpenDoor;


	}

	public void Clear()
	{
		MicrowaveON = false;
		TimeM = 0;

	}

//	void OnTriggerEnter(Collider col)
//	{
//		if(col.tag == ("Player"))
//		playerCon.microwaveRange = true;
//	}
//
//	void OnTriggerExit (Collider col)
//	{
//        if (col.tag == ("Player"))
//        {
////            playerCon.microwaveRange = false;
////            playerCon.microwaveMode = false;
//        }

//	}
}
