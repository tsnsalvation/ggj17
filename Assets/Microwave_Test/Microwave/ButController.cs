﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButController : MonoBehaviour {
	public enum ButtonType
	{
		Keypad,
		Eject,
		On,
		Clear,
		CloseDoor
	}
	public int timeValue;
	public ButtonType thisButton; 
	public MicrowaveController micScript;
	public bool active;

	// Use this for initialization
	void Start () {
//		micScript = GameObject.Find ("MicrowaveBase").GetComponent<MicrowaveController> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ActivateBut()
	{
		if (!active) 
		{
			switch (thisButton) 
			{
			case ButtonType.Keypad:
				micScript.addTime (timeValue);
				break;
			case ButtonType.On:
				micScript.TurnOn ();
				break;
			case ButtonType.Eject:
				micScript.Eject ();
				break;

			case ButtonType.Clear:
				micScript.Clear ();
				break;
			case ButtonType.CloseDoor:
				micScript.CloseDoor ();
				break;

			}
//			active = true;
		}
	}
}
