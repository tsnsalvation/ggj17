﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupboardDoor : MonoBehaviour {
	public enum CupboardType
	{
		Door,
		Drawer,
		DumbDoor,
	}
	public CupboardType cupType;

	public int DoorRot;
	public float DrawMove;
	public Vector3 DoorTestX;
	int DoorTesty;
	public bool Open;
	public float doorSpeed;
	float saveSpeed;
	// Use this for initialization
	void Start () {
		
		saveSpeed = doorSpeed;
		switch (cupType) 
		{
		case CupboardType.Door:
			DoorTestX = this.gameObject.transform.eulerAngles;
			break;
		case CupboardType.Drawer:
			DoorTestX = this.gameObject.transform.position;
			break;
		case CupboardType.DumbDoor:
			DoorTestX = this.gameObject.transform.position;
			break;
		}



	}
	
	// Update is called once per frame
	void Update () {
		if (Open) 
		{
			switch (cupType) 
			{
			case CupboardType.Door:
				this.gameObject.transform.rotation = Quaternion.Lerp (this.gameObject.transform.rotation, Quaternion.Euler(new Vector3(DoorTestX.x,DoorRot,DoorTestX.z)), Time.deltaTime * doorSpeed);
				break;
			case CupboardType.Drawer:
				if (DoorRot == 2) 
				{
					this.gameObject.transform.position = Vector3.Lerp (this.gameObject.transform.position, new Vector3 (DrawMove, DoorTestX.y, DoorTestX.z), Time.deltaTime * doorSpeed);
				} 
				else 
				{
					this.gameObject.transform.position = Vector3.Lerp (this.gameObject.transform.position, new Vector3 (DoorTestX.x, DoorTestX.y, DrawMove), Time.deltaTime * doorSpeed);
				}
				break;
			case CupboardType.DumbDoor:
				this.gameObject.transform.position = Vector3.Lerp (this.gameObject.transform.position, new Vector3 (DoorTestX.x, DrawMove, DoorTestX.z), Time.deltaTime * doorSpeed);
				break;
			}
		} 
		else 
		{
			switch (cupType) 
			{
			case CupboardType.Door:
				this.gameObject.transform.rotation = Quaternion.Lerp (this.gameObject.transform.rotation, Quaternion.Euler(DoorTestX),Time.deltaTime * doorSpeed);				
				break;
			case CupboardType.Drawer:
				this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, DoorTestX, Time.deltaTime * doorSpeed) ;
				break;
			case CupboardType.DumbDoor:
				this.gameObject.transform.position = Vector3.Lerp (this.gameObject.transform.position, DoorTestX, Time.deltaTime * doorSpeed);
					break;
			}

		}
		
	}

	public void DoorSwitch()
	{
		Open = !Open;
		if (doorSpeed <= 0)
		doorSpeed = saveSpeed;
	}

//	void OnCollisionEnter(Collision col)
//	{
//		if (col.gameObject.tag == "Player")
//			doorSpeed = 0;
//
//		if (col.gameObject.GetComponent<CupboardDoor>() != null) 
//		{
//			if(col.gameObject.GetComponent<CupboardDoor>().Open)
//			doorSpeed = 0;
//
//		}
//			
//
//	
//	}
}
