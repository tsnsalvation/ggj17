﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderForm : MonoBehaviour
{
    public ShoppingHud shopHud;

    public void TriggerHud()
    {
        shopHud.EnableShopping();
    }

}
