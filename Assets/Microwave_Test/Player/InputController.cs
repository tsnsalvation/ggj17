﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
	public bool mouseInput;
	public bool interactInput;
	public float horizontal;
	public float vertical;
	public float mouseX;
	public float mouseY;
	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {
		mouseInput = Input.GetMouseButtonDown(0);
		interactInput = Input.GetButton ("Fire2");
		horizontal = Input.GetAxis ("Horizontal");
		vertical = Input.GetAxis ("Vertical");
		mouseX = Input.GetAxis ("Mouse X");
		mouseY = Input.GetAxis ("Mouse Y");
	}
}
