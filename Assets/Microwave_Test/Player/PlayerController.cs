﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class PlayerController : MonoBehaviour
{
    public bool microwaveMode;
    public bool microwaveRange;
    public GameObject holdingObject;
    Transform Objectholder;
    public bool holding;
    public GameObject ObjectlookingAt;
    public float distObjLook;

    public float Speed = 5f;
    Vector3 moveDir;
    CharacterController charController;
    public float playReach;
    InputController inputcontroller;
    public MicrowaveController micController;
    HudManager hudMan;
    UnityStandardAssets.Characters.FirstPerson.FirstPersonController fpsScript;
    
   
    // Use this for initialization
    void Start()
    {
        charController = this.gameObject.GetComponent<CharacterController>();
        inputcontroller = this.gameObject.GetComponent<InputController>();
        Objectholder = GameObject.Find("ObjectHolder").transform;
        fpsScript = this.gameObject.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        hudMan = GameObject.Find("HUD").GetComponent<HudManager>();
        
    }

    // Update is called once per frame
    void Update()
    {

        //		if (!microwaveMode) 
        //		{
        //			fpsScript.enabled = true;
        //            Cursor.lockState = CursorLockMode.Confined;
        //
        //        } 
        //		else 
        //		{
        //			fpsScript.enabled = false;
        //            Cursor.lockState = CursorLockMode.None;
        //        }

        checkRay();
        Checkinput();
    }
    void RotateView()
    {


    }

    void MoveChar()
    {
        moveDir = new Vector3(inputcontroller.horizontal, 0, inputcontroller.vertical);
        moveDir = transform.TransformDirection(moveDir);
        moveDir *= Speed;

        charController.Move(moveDir);

    }

    void Checkinput()
    {
        if (inputcontroller.mouseInput && ObjectlookingAt != null && distObjLook <= playReach)
        {

            if (ObjectlookingAt.tag == ("Interact"))
            {
                if (ObjectlookingAt.GetComponent<ButController>() != null)
                {
                    if (ObjectlookingAt.transform.parent.name == "ButtonHolder" || ObjectlookingAt.name == "Door")
                    {
                        ButController MicButton = ObjectlookingAt.GetComponent<ButController>();
                        MicButton.ActivateBut();
                    }

                }
                else
                {

                    if (ObjectlookingAt.name == "MicrowaveBase" && !micController.empty && micController.OpenDoor)
                    {
                        if (holdingObject)
                        {
                            if (micController.micSpots[0] != null && micController.micSpots[1] != null && micController.micSpots[2] != null)
                            {

                            }
                            else
                            {
                                micController.addObject(holdingObject, 0);
                                holdingObject = null;
                                holding = false;

                            }
                        }
                        else
                        {

                            if (micController.micSpots[0] != null)
                            {
                                holdingObject = micController.micSpots[0];
                                micController.micSpots[0] = null;
                            }
                            else if (micController.micSpots[1] != null)
                            {
                                holdingObject = micController.micSpots[1];
                                micController.micSpots[1] = null;
                            }
                            else if (micController.micSpots[2] != null)
                            {
                                holdingObject = micController.micSpots[2];
                                micController.micSpots[1] = null;
                            }
                            holdingObject.transform.position = Objectholder.position;
                            holdingObject.transform.parent = Objectholder;
                            Rigidbody heldRigid = holdingObject.GetComponent<Rigidbody>();
                            heldRigid.isKinematic = true;
                            holding = true; 
                        }
                    }
                    else if (ObjectlookingAt.GetComponent<CupboardDoor>() != null)
                    {
                        CupboardDoor cupController;
                        cupController = ObjectlookingAt.GetComponent<CupboardDoor>();
                        cupController.DoorSwitch();

                    }
                    else if (ObjectlookingAt.GetComponent<DumbBut>() != null)
                    {
                        DumbBut dumbButt;
                        dumbButt = ObjectlookingAt.GetComponent<DumbBut>();
                        dumbButt.openDoor();
                    }
                    else if (ObjectlookingAt.GetComponent<OrderForm>() != null)
                    {
                        var controller = ObjectlookingAt.GetComponent<OrderForm>();
                        controller.TriggerHud();
                    }
                    else if (ObjectlookingAt.name == "MicrowaveBase" && holding)
                    {
                        micController = ObjectlookingAt.GetComponent<MicrowaveController>();
                        if (micController.OpenDoor)
                        {
                            if (micController.micSpots[0] != null && micController.micSpots[1] != null && micController.micSpots[2] != null)
                            {

                            }
                            else
                            {
                                micController.addObject(holdingObject, 0);
                                holdingObject = null;
                                holding = false;

                            }
                        }
                    }
                    else if (ObjectlookingAt.name == "DropOffBox" && holding)
                    {
                        DropOffController dropCon;
                        dropCon = ObjectlookingAt.GetComponent<DropOffController>();
                        if (dropCon.OpenDoor)
                        {
                            
                            Rigidbody heldRigid = holdingObject.GetComponent<Rigidbody>();
                            heldRigid.isKinematic = false;
                            dropCon.AddtoBox(holdingObject);
                            holdingObject = null;
                            holding = false;
                        }

                    }
                }
            }
            else if (ObjectlookingAt.tag == ("Product") && !holding)
            {
                holdingObject = ObjectlookingAt;
                holdingObject.transform.position = Objectholder.position;
                holdingObject.transform.parent = Objectholder;
                Rigidbody heldRigid = holdingObject.GetComponent<Rigidbody>();
                heldRigid.isKinematic = true;
                holding = true;

            }

        }
        else if (inputcontroller.mouseInput && holdingObject)
        {
            if (ObjectlookingAt == null)
            {
                holdingObject.transform.parent = null;
                Rigidbody heldRigid = holdingObject.GetComponent<Rigidbody>();
                heldRigid.isKinematic = false;
                holdingObject = null;
                holding = false;

            }
            else if (ObjectlookingAt.name == ("MicrowaveBase"))
            {
                micController = ObjectlookingAt.GetComponent<MicrowaveController>();
                if (micController.OpenDoor)
                {
                    if (micController.micSpots[0] != null && micController.micSpots[1] != null && micController.micSpots[2] != null)
                    {

                    }
                    else
                    {
                        micController.addObject(holdingObject, 0);
                        holdingObject = null;
                        holding = false;

                    }
                }
            }
            else if (ObjectlookingAt.name == ("DropOffBox"))
            {
                DropOffController dropCon;
                dropCon = ObjectlookingAt.GetComponent<DropOffController>();
                if (dropCon.OpenDoor)
                {
                    
                    Rigidbody heldRigid = holdingObject.GetComponent<Rigidbody>();
                    heldRigid.velocity = Vector3.zero;
                    heldRigid.drag = 10;
                    heldRigid.isKinematic = false;
                    dropCon.AddtoBox(holdingObject);
                    holdingObject = null;
                    holding = false;
                }




            }
            else
            {
                holdingObject.transform.parent = null;
                Rigidbody heldRigid = holdingObject.GetComponent<Rigidbody>();
                heldRigid.isKinematic = false;
                holdingObject = null;
                holding = false;

            }
        }
        if (inputcontroller.interactInput && ObjectlookingAt != null)
        {
            //			if (ObjectlookingAt.name == ("MicrowaveBase")) 
            //			{
            //				if (!microwaveMode) 
            //				{
            //					
            //					microwaveMode = true;
            //					
            //				} 
            //				else 
            //				{
            //					microwaveMode = false;
            //
            //				}
            //
            //			} 
            //			else 
            //			{
            //
            //
            //			}
        }
    }


    void checkRay()
    {
        RaycastHit rayHit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out rayHit))
        {
            if (rayHit.collider.tag == ("Interact") || rayHit.collider.tag == ("Product"))
            {
                ObjectlookingAt = rayHit.collider.gameObject;
                distObjLook = rayHit.distance;
                hudMan.cursorInteractive = true;

            }
            else
            {
                hudMan.cursorInteractive = false;
                ObjectlookingAt = null;
                distObjLook = 0;
            }

        }

    }
    
}
