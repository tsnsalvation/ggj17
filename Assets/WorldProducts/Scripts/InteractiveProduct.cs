﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveProduct : MonoBehaviour
{
    public Product thisProduct;
	public byte productState; // raw, cooked, burnt
    public int worth;
    public MeshFilter myMesh;
    
	public float cookTime;

    public string productResourceMesh = "Mesh/";
    public string productResourceMat = "Materials/";


    public void Initialise(Product prod)
    {
        thisProduct = prod;
        worth = prod.productCost;
        GetMesh();
    }

    void GetMesh()
    {
        Mesh mesh;
        Material[] mat;

        mesh = Resources.Load<Mesh>(productResourceMesh + thisProduct.productName);
        mat = Resources.LoadAll<Material>(productResourceMesh +productResourceMat + thisProduct.productName);

        GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<MeshRenderer>().materials = mat;
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }
}
