﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestList  {

    public List<Quests> quests;
    public QuestList()
    {
        quests = new List<Quests>();
    }
}
[System.Serializable]
public class Quests
{
    public Product product;
    public int quantity;
    public int bonus;
}
