﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoppingOrder : MonoBehaviour
{

    public List<Product> orderedProducts;
    public List<Product> suppliedProducts;

    public List<InitialOrderItem> initialOrder = new List<InitialOrderItem>();

    public AnimationCurve chanceOfSubstituion;

    public WorldProducts productDatabase;

    public void DetermineSupply()
    {
        suppliedProducts.Clear();

        var orderSize = orderedProducts.Count;//GetOrderSize(orderedProducts);
        var substitionThreshold = chanceOfSubstituion.Evaluate(orderSize);
        foreach (var p in orderedProducts)
        {
            //for (int i = 0; i < p.qty; i++)
            //{
                var diceRoll = Random.Range(0, 100);
                if (diceRoll < substitionThreshold)
                {
                    var productSupplied = Random.Range(0, productDatabase.products.Count - 1);
                    suppliedProducts.Add(productDatabase.products[productSupplied]);
                }
                else
                {
                    suppliedProducts.Add(p);
                } 
            //}
        }
    }
    public int GetOrderSize(List<InitialOrderItem> order)
    {
        int sum = 0;
        for (int i = 0; i < order.Count; i++)
        {
            sum += order[i].qty;
        }
        return sum;
    }
    public void OrderiseInitial()
    {
        foreach (var p in initialOrder)
        {
            for (int i = 0; i < p.qty; i++)
            {
                orderedProducts.Add(p.product);
            }
        }
    }
}



[System.Serializable]
public class InitialOrderItem
{
    public Product product;
    public int qty;
}