﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections.Generic;


public class GameManager : MonoBehaviour
{
    public float gameStartPhaseTime;
    public float gameCookingRoundTime;
    public float gameReorderStockTime;
    public float transitionTime;

    public bool respawnProduct;

    ProductSpawner spawner;

    public ShoppingOrder order;

    public QuestList intialQuests;// = new QuestList();
    public QuestList allQuests;

    public QuestList currentQuest;

    public QuestHud questHud;

    public float currentTimer;
    public float CurrentTimer
    {
        get
        {
            return currentTimer;
        }
        set
        {
            currentTimer = value;
            hudMgr.UpdateTime(value);
        }
    }

    int currentMoney;
    public int CurrentMoney
    {
        get
        {
            return currentMoney;
        }
        set
        {
            currentMoney = value;
            hudMgr.UpdateMoney(value);
        }
    }
    public GameState currentState;

    public int roundNumber = 0;

    public HudManager hudMgr;

    public bool loser = false;
    public int requireRoundMoney;
    public FirstPersonController fps;
    public PlayerController playerCon;
    public InputController inputController;

    public GameObject productOrder;

    public DropOffController dropsiesPop;


    void Start()
    {
        CurrentTimer = gameStartPhaseTime;
        spawner = GetComponent<ProductSpawner>();
        //Initial scene start spawn some random products.
        //get products
        if (order.initialOrder != null)
        {
            try
            {
                order.OrderiseInitial();
                spawner.SpawnProducts(order.orderedProducts);
            }
            catch
            {

            }
        }
        //get tasks
        currentQuest = intialQuests;
        questHud.BuildQuestText(currentQuest);
        CurrentMoney = 50;
        productOrder.SetActive(false);
    }
    void Update()
    {
        if (loser)
            return;
        CurrentTimer -= Time.deltaTime;

        if (currentState == GameState.Starting && currentTimer <= 0.0f)
        {
            //End of prestart phase change any settings here
            CurrentTimer = gameCookingRoundTime;
            currentState = GameState.Cooking;
            order.orderedProducts.Clear();
            hudMgr.UpdatePhase(GameState.Cooking.ToString());
            roundNumber++;
            hudMgr.UpdateRound(roundNumber);
            SetRoundGoal();
        }

        if (currentState == GameState.Cooking && currentTimer <= 0.0f)
        {
            //End of cooking phase
            CurrentTimer = gameReorderStockTime;
            currentState = GameState.Ordering;
            hudMgr.UpdatePhase(GameState.Ordering.ToString());
            CalculateEarnings();
            if (CheckLosingConditions())
                return;
            SetNewTasks();
            questHud.BuildQuestText(currentQuest);
            SetRoundGoal();
            productOrder.SetActive(true);
        }

        if (currentState == GameState.Ordering && currentTimer <= 0.0f)
        {
            //end of ordering phase 
            currentTimer = transitionTime;
            currentState = GameState.Smoko;
            respawnProduct = true;
            hudMgr.UpdatePhase(GameState.Smoko.ToString());
            order.DetermineSupply();
            productOrder.SetActive(false);

        }
        if (currentState == GameState.Smoko)
        {

            if (respawnProduct && currentTimer <= transitionTime / 2)
            {
                spawner.SpawnProducts(order.suppliedProducts);
                order.orderedProducts.Clear();
                order.suppliedProducts.Clear();
                respawnProduct = false;
            }
            if (currentTimer < 0)
            {
                currentTimer = gameCookingRoundTime;
                currentState = GameState.Cooking;
                respawnProduct = true;
                hudMgr.UpdatePhase(GameState.Cooking.ToString());
                roundNumber++;

                questHud.SetMoneyRequired(requireRoundMoney);
                hudMgr.UpdateRound(roundNumber);
            }

        }
    }
    public enum GameState
    {
        Starting,
        Cooking,
        Ordering,
        Smoko
    }

    public void CalculateEarnings()
    {
        int totalEarnings = 0;
        var soldProducts = dropsiesPop.GetList();

        // Calculate quest bonus
        foreach (var q in currentQuest.quests)
        {
            int totalMatchingProducts =0;
            for (int i = 0; i < soldProducts.Count; i++)
            {
                if (q.product ==soldProducts[i].thisProduct)
                {
                    totalMatchingProducts++;
                }
                
            }
            if (totalMatchingProducts >= q.quantity)
            {
                //Debug.Log(q.bonus);
                totalEarnings += q.bonus;
            }
        }


        // calculate all products sold
        for (int i = 0; i < soldProducts.Count; i++)
        {
            totalEarnings += soldProducts[i].worth;
        }

        CurrentMoney += totalEarnings;
        dropsiesPop.CleanBox();
    }

    public void SetNewTasks()
    {
        currentQuest.quests.Clear();
        for (int i = 0; i < 3; i++)
        {
            var questIndex = Random.Range(0, allQuests.quests.Count);
            currentQuest.quests.Add(allQuests.quests[questIndex]);
        }
    }
    bool CheckLosingConditions()
    {
        if (currentMoney < requireRoundMoney)
        {
            loser = true;
            hudMgr.GameOver();
            DisablePlayer();
            return true;
        }
        return false;
    }
    void SetRoundGoal()
    {
        requireRoundMoney = roundNumber * 10;
    }

    public void DisablePlayer()
    {
        fps.enabled = false;
        playerCon.enabled = false;
        inputController.enabled = false;
        hudMgr.cursor.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void EnablePlayer()
    {
        fps.enabled = true;
        playerCon.enabled = true;
        inputController.enabled = true;
        hudMgr.cursor.enabled = true;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }
}
