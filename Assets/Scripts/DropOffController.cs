﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropOffController : MonoBehaviour {
	public List<InteractiveProduct> products = new List<InteractiveProduct>();
	public Transform productHolder;
	public bool OpenDoor;
	public CupboardDoor doorScript;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		OpenDoor = doorScript.Open;
	}

	public void AddtoBox(GameObject productToAdd)
	{
		
		productToAdd.transform.position = productHolder.position;
		productToAdd.transform.rotation = Quaternion.Euler (0, 0, 0);
		productToAdd.transform.parent = productHolder;
		products.Add (productToAdd.GetComponent<InteractiveProduct> ());

	}



	public List<InteractiveProduct>GetList()
	{
		return products;
	}

	[ContextMenu ("Clean Box")]
	public void CleanBox()
	{
		products.Clear();

		foreach (Transform child in productHolder.transform) 
		{
			GameObject.Destroy (child.gameObject);

		}

	}
}
