﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductSpawner : MonoBehaviour
{
    public Transform defaultProductSpawn;
    public List<Transform> productSpawnLocations;
    public GameObject productPrefab;

    public int spawnCheckAttempts = 3;

    public void SpawnProducts(List<Product> products)
    {
        foreach (var p in products)
        {
            var spawnLocation = defaultProductSpawn.position;
            //get a safe spawn
            for (int i = 0; i < spawnCheckAttempts; i++)
            {
                var spawnPoint = Random.Range(0, productSpawnLocations.Count - 1);
                if (CanSpawnHere(productSpawnLocations[spawnPoint].position))
                {
                    spawnLocation = productSpawnLocations[spawnPoint].position;
                    break;
                }
            }
            SpawnProduct(p, spawnLocation);

        }

    }

    public void SpawnProducts(List<InitialOrderItem> products)
    {
        foreach (var p in products)
        {
            for (int i = 0; i < p.qty; i++)
            {

                var spawnLocation = defaultProductSpawn.position;
                //get a safe spawn
                if (productSpawnLocations.Count > 0)
                {

                    for (int j = 0; j < spawnCheckAttempts; j++)
                    {
                        var spawnPoint = Random.Range(0, productSpawnLocations.Count - 1);
                        if (CanSpawnHere(productSpawnLocations[spawnPoint].position))
                        {
                            spawnLocation = productSpawnLocations[spawnPoint].position;
                            break;
                        }
                    }
                }
                SpawnProduct(p.product, spawnLocation);

            }
        }

    }


    public bool CanSpawnHere(Vector3 spawnPoint)
    {
        if (Physics.CheckSphere(spawnPoint, 0.2f))
            return false;
        return true;
    }

    void SpawnProduct(Product product, Vector3 spawn)
    {
        var go = Instantiate(productPrefab, spawn, Quaternion.identity);
        var prod = go.GetComponent<InteractiveProduct>();
        prod.Initialise(product);
    }

}
